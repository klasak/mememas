# coding=utf-8
import logging
import os
import sys
import Pyro4
from pyage.core import address
from pyage.core.agent.agent import generate_agents, Agent
from pyage.core.locator import TorusLocator
from pyage.core.migration import ParentMigration
from core.statistics import TimeStatistics
from core.stop_condition import TimeLimitStopCondition
from solutions.evolution.crossover import SinglePointListCrossover
from solutions.evolution.evaluation import LABSEvaluation
from solutions.evolution.initializer import ListInitializer
from solutions.evolution.mutation import UniformListMutation
from pyage.solutions.evolution.selection import TournamentSelection

logger = logging.getLogger(__name__)

if len(sys.argv) == 6:
	agents_count = int(sys.argv[2])
	max_time = int(sys.argv[3])
	problem_size = int(sys.argv[4])
	mutation_probability = float(sys.argv[5])
else:
	agents_count = int(os.environ['AGENTS'])
	max_time = 10
	problem_size = 100
	population_size = 100
	mutation_probability = 0.02

population_size = 100
torus_x = 12

logger.debug("EVO, %s agents", agents_count)
agents = generate_agents("agent", agents_count, Agent)

stop_condition = lambda: TimeLimitStopCondition(max_time)

operators = lambda: [LABSEvaluation(), TournamentSelection(size=population_size/2,
    tournament_size=2), SinglePointListCrossover(size=population_size), UniformListMutation(mutation_probability)]

initializer = lambda: ListInitializer(problem_size, population_size)
address_provider = address.SequenceAddressProvider
migration = ParentMigration

address_provider = address.SequenceAddressProvider
params = lambda: 'AC{0}MT{1}PRS{7}POS{8}MP{9}'.format(agents_count(), max_time(), problem_size(), population_size(), mutation_probability())

locator = lambda: TorusLocator(x=torus_x, y=torus_x)

stats = TimeStatistics
