from pyage.core.operator import Operator
from solutions.evolution.genotype import ListGenotype

class LABSEvaluation(Operator):
	def __init__(self):
		super(LABSEvaluation, self).__init__(ListGenotype)

	def process(self, population):
		for genotype in population:
			genotype.fitness = -self.LABSenergy(genotype.genes)

	def LABSenergy(self, genes):
		sum = 0
		for k in range(1,len(genes)):
			sum += self.autocorrelation(k, genes) ** 2
		return sum

	def autocorrelation(self, k, genes):
		sum = 0
		for i in range(0, len(genes)-k):
			sum += genes[i] * genes[i+k]
		return sum
