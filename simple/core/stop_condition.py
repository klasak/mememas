import logging
import Pyro4
import time
from pyage.core.stop_condition import StopCondition


class TimeLimitStopCondition(StopCondition):
	def __init__(self, time_limit):
		super(TimeLimitStopCondition, self).__init__()
		self.start_time = self.current_time()
		self.time_limit = time_limit

	def should_stop(self, workplace):
		return self.current_time() - self.start_time >= self.time_limit

	def current_time(self):
		return int(round(time.time()))
