import unittest
from solutions.mem_evolution.genotype import MemListGenotype
from solutions.mem_evolution.evaluation import MemLABSEvaluation

class TestEnergyEval(unittest.TestCase):

	def setUp(self):
		pass

	def test_short(self):
		genotype = MemListGenotype([-1, 1, 1, -1])		
		self.assertEqual(genotype.energy,6)

	def test_long(self):
		genotype = MemListGenotype([1,-1,1,-1,-1,1,-1,1,1,-1,1,-1,-1,-1,1,1,1,1,-1,-1,-1,1,-1,-1,-1,-1,1,1,1,-1,1,1,1,-1,1,1,1,-1,1,1])
		self.assertEqual(genotype.energy,108)

	def test_equivalent(self):
		genotype1 = MemListGenotype([-1, 1, 1, -1, 1, 1, 1, -1, 1, -1])
		genotype2 = MemListGenotype([1, -1, -1, 1, -1, -1, -1, 1, -1, 1])
		self.assertEqual(genotype1.energy, genotype2.energy)

	def test_reversed(self):
		genotype1 = MemListGenotype([-1, -1, -1, -1, 1, 1, 1, -1, 1, -1])
		genotype2 = MemListGenotype([-1, 1, -1, 1, 1, 1, -1, -1, -1, -1])
		self.assertEqual(genotype1.energy, genotype2.energy)

        def test_flip(self):
                genotype = MemListGenotype([-1, -1, -1, -1, 1, 1, 1, -1, 1, -1])
                genotype0 = MemListGenotype([1, -1, -1, -1, 1, 1, 1, -1, 1, -1])
                genotype9 = MemListGenotype([-1, -1, -1, -1, 1, 1, 1, -1, 1, 1])
                genotype4 = MemListGenotype([-1, -1, -1, -1, -1, 1, 1, -1, 1, -1])

                self.assertEqual(genotype.flip(0), genotype0.energy)
                self.assertEqual(genotype.flip(9), genotype9.energy)
                self.assertEqual(genotype.flip(4), genotype4.energy)

if __name__ == '__main__':
	unittest.main()
