from pyage.core.operator import Operator
from solutions.mem_evolution.genotype import MemListGenotype

class MemLABSEvaluation(Operator):
	def __init__(self):
		super(MemLABSEvaluation, self).__init__(MemListGenotype)

	def process(self, population):
		for genotype in population:
			genotype.fitness = -genotype.energy


