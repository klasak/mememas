import random
from pyage.solutions.evolution.mutation import AbstractMutation
from solutions.mem_evolution.genotype import MemListGenotype

class MemListMutation(AbstractMutation):
	def __init__(self, probability, maxIterations):
		super(MemListMutation, self).__init__(MemListGenotype, 1)
		self.mutationProbability = probability
		self.maxIterations = maxIterations

	def mutate(self, genotype):
		if random.random() < self.mutationProbability:
			index = random.randint(0, len(genotype.genes)-1)
			genotype.genes[index] = -genotype.genes[index]
			genotype.genesChanged()
		self.optimize(genotype)

	def optimize(self, genotype):
		for iteration in range(0, self.maxIterations):
			minEnergy = genotype.energy
			minIndex = -1
			for i in range(0,len(genotype.genes)-1):
				candidateEnergy = genotype.flip(i)
				if (candidateEnergy < minEnergy):
					minEnergy = candidateEnergy
					minIndex = i
			if (minEnergy < genotype.energy):
				genotype.genes[minIndex] = -genotype.genes[minIndex]
				genotype.genesChanged()
			else:
				return

