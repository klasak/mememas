class MemListGenotype(object):
	def __init__(self, genes):
		super(MemListGenotype, self).__init__()
		self.fitness = None
		self.energy = None
		self.genes = genes
		self.products = [[0 for x in range(len(self.genes)-1)] for x in range(len(self.genes)-1)]
		self.correlations = [0 for x in range(len(self.genes)-1)]
		self.genesChanged()

	def genesChanged(self):
		self.computeProducts()
		self.computeCorrelations()
		self.computeEnergy()

	def computeProducts(self):
		for i in range(0,len(self.genes)-1):
			for j in range(0, len(self.genes)-i-1):
				self.products[j][i] = self.genes[i]*self.genes[j+i+1]
				
	def computeCorrelations(self):
		for i in range(0,len(self.genes)-1):
			sum = 0
			for j in range(0, len(self.genes)-i-1):
				sum += self.products[i][j]
			self.correlations[i] = sum

	def computeEnergy(self):
		sum = 0
		for i in range(0,len(self.correlations)):
			sum += self.correlations[i] ** 2
		self.energy = sum
			
	def flip(self, i):
		f = 0
		for p in range(0,len(self.genes)-1):
			v = self.correlations[p]
			if (p < len(self.genes)-i-1):
				v = v - 2*self.products[p][i]
			if (p < i):
				v = v - 2*self.products[p][i-p-1]
			f = f + v**2
		return f

	def __str__(self):
		return "%s, f:%s" % (self.genes, self.fitness)

	def __repr__(self):
		return self.__str__()
