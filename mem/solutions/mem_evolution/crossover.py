import random
from pyage.core.operator import Operator
from solutions.mem_evolution.genotype import MemListGenotype

class AbstractCrossover(Operator):
	def __init__(self, type, size):
		super(AbstractCrossover, self).__init__(type)
		self.__size = size

	def process(self, population):
		parents = list(population)
		for i in range(len(population), self.__size):
			p1, p2 = random.sample(parents, 2)
			genotype = self.cross(p1, p2)
			population.append(genotype)


class SinglePointListCrossover(AbstractCrossover):
	def __init__(self, size):
		super(SinglePointListCrossover, self).__init__(MemListGenotype, size)


	def cross(self, p1, p2):
		crossingPoint = random.randint(1, len(p1.genes))
		return MemListGenotype(p1.genes[:crossingPoint] + p2.genes[crossingPoint:])
