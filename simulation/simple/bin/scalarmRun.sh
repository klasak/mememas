#!/bin/bash

# Envs:
#
# AmountOfIterations
# AC 
# MT
# PRS
# MP

BIN_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
if [ -z "$PYTHONPATH" ]; then
	PYTHONPATH=$BIN_DIR
else
	PYTHONPATH=$PYTHONPATH:$BIN_DIR
fi

mkdir emas
mkdir emas/logs
mkdir genetic
mkdir genetic/logs
mkdir plots

touch parameters
echo -e "{\n'agents_count': "$AC",\n'max_time': "$MT",\n'problem_size': "$PRS",\n'mutation_probability': "$MP"\n}" > ./parameters

mkdir Output
for i in `seq 1 $AmountOfIterations`;
do
	python -m pyage.core.bootstrap conf.genetic $AC $MT $PRS $MP
	FILE1=$(find . -maxdepth 1 -name "*.txt" -not -name "_stdout.txt")
	mv $FILE1 Output/sim_$i
done

mv *.log genetic/logs
mv Output genetic/

mkdir Output
for i in `seq 1 $AmountOfIterations`;
do
	python -m pyage.core.bootstrap conf.emas $AC $MT $PRS $MP
	FILE1=$(find . -maxdepth 1 -name "*.txt" -not -name "_stdout.txt")
	mv $FILE1 Output/sim_$i
done

mv *.log emas/logs
mv Output emas/

python $BIN_DIR/plot.py

mv *.png plots

python $BIN_DIR/summary.py  > ./summary

mkdir simulation
mv emas simulation
mv genetic simulation
mv plots simulation

mv parameters simulation
mv summary simulation

rm -f *.pyc
