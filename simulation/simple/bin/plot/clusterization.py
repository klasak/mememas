def clusterize(data, k, key):
	res = []
	xprev = 0
	i = 0
	for (x, y) in data:
		if x < xprev:
			i = 0
		if i >= len(res) and i < k:
			res.append([])
		if i < k:
			res[i].append((x, y))
		xprev = x
		i += 1

	return res


def partition(data, k, key=lambda x: x):
	clusters = clusterize(data, k, key)

	for cluster in clusters:
		xs = [x for (x, _) in cluster]
		x = sum(xs) / float(len(xs))
		yield (x, [y for (_, y) in cluster])

