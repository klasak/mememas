#!/usr/bin/env python
# encoding: utf-8

from plot.data import get_data_from_dir, process_data
from plot.plot import plot_data
from plot.clusterization import partition


out_dir = 'Output/'
emas_dir = 'emas/' + out_dir
gen_dir = 'genetic/' + out_dir

f = open('parameters')
parameters = eval(f.read())
f.close()
print parameters

def prepare(data_dir):
	data, k = get_data_from_dir(data_dir)
	# data.sort(key=lambda (t, _): t)
	parts = partition(data, k)
	return list(process_data(parts))


emas = prepare(emas_dir)
gen = prepare(gen_dir)

plot_data(parameters, emas=emas, genetic=gen)
