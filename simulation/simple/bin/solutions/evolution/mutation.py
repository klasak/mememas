import random
from pyage.solutions.evolution.mutation import AbstractMutation
from solutions.evolution.genotype import ListGenotype

class UniformListMutation(AbstractMutation):
	def __init__(self, probability):
		super(UniformListMutation, self).__init__(ListGenotype, probability)

	def mutate(self, genotype):
		index = random.randint(0, len(genotype.genes)-1)
		genotype.genes[index] = -genotype.genes[index]

