import unittest
from solutions.evolution.evaluation import LABSEvaluation

class TestEnergyEval(unittest.TestCase):

	def setUp(self):
		self.evaluation = LABSEvaluation()

	def test_short(self):
		genes = [-1, 1, 1, -1]
		self.assertEqual(self.evaluation.LABSenergy(genes),6)

	def test_long(self):
		genes = [1,-1,1,-1,-1,1,-1,1,1,-1,1,-1,-1,-1,1,1,1,1,-1,-1,-1,1,-1,-1,-1,-1,1,1,1,-1,1,1,1,-1,1,1,1,-1,1,1]
		self.assertEqual(self.evaluation.LABSenergy(genes),108)

	def test_equivalent(self):
		genes1 = [-1, 1, 1, -1, 1, 1, 1, -1, 1, -1]
		genes2 = [1, -1, -1, 1, -1, -1, -1, 1, -1, 1]
		self.assertEqual(self.evaluation.LABSenergy(genes1),self.evaluation.LABSenergy(genes2))

	def test_reversed(self):
		genes1 = [-1, -1, -1, -1, 1, 1, 1, -1, 1, -1]
		genes2 = [-1, 1, -1, 1, 1, 1, -1, -1, -1, -1]
		self.assertEqual(self.evaluation.LABSenergy(genes1),self.evaluation.LABSenergy(genes2))

if __name__ == '__main__':
	unittest.main()
