from random import choice
from pyage.core.emas import EmasAgent
from pyage.core.operator import Operator
from solutions.evolution.genotype import ListGenotype


class ListInitializer(Operator):
	def __init__(self, dims, size):
		super(ListInitializer, self).__init__(ListGenotype)
		self.size = size
		self.dims = dims

	def process(self, population):
		for i in range(self.size):
			population.append(ListGenotype([self.__randomize() for _ in range(self.dims)]))

	def __randomize(self):
		return choice([-1, 1]);

def list_emas_initializer(dims,energy, size):
	agents = {}
	for i in range(size):
		agent = EmasAgent(ListGenotype([choice([-1, 1]) for _ in range(dims)]), energy)
		agents[agent.get_address()] = agent
	return agents

