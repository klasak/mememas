#!/usr/bin/env python
# encoding: utf-8

from os import listdir
from plot.data import parse_file

out_dir = 'Output/'
emas_dir = 'emas/' + out_dir
gen_dir = 'genetic/' + out_dir

f = open('parameters')
parameters = eval(f.read())
f.close()


def format_csv(dict, *keys):
	return ';'.join(str(dict[k]) for k in keys)

def process(**dirs):
	for name, dir in dirs.iteritems():
		for f in listdir(dir):
			path = dir + f
			data = list(parse_file(path))
			vals = [v for (_, v) in data]
			yield dict({
				'type': name,
				'sim': f,
				'best_found': min(vals),
			}, **parameters)

fields = [
	'type', 'agents_count', 'problem_size', 'mutation_probability',
	'max_time', 'sim', 'best_found'
]

for line in process(emas=emas_dir, gen=gen_dir):
	print format_csv(line, *fields)
