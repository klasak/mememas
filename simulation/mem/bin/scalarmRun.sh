#!/bin/bash

# Envs:
#
# AmountOfIterations
# AC 
# MT
# MOI
# PRS
# MP

BIN_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
if [ -z "$PYTHONPATH" ]; then
	PYTHONPATH=$BIN_DIR
else
	PYTHONPATH=$PYTHONPATH:$BIN_DIR
fi

mkdir rmhc
mkdir rmhc/logs
mkdir sdls
mkdir sdls/logs
mkdir ts
mkdir ts/logs

mkdir plots

touch parameters
echo -e "{\n'agents_count': "$AC",\n'max_time': "$MT",\n'max_opt_iterations': "$MOI",\n'problem_size': "$PRS",\n'mutation_probability': "$MP"\n}" > ./parameters

# RMHC
###############
mkdir Output
for i in `seq 1 $AmountOfIterations`;
do
	python -m pyage.core.bootstrap conf.mememas_rmhc $AC $MT $MOI $PRS $MP
	FILE1=$(find . -maxdepth 1 -name "*.txt" -not -name "_stdout.txt")
	mv $FILE1 Output/sim_$i
done

mv *.log rmhc/logs
mv Output rmhc/
###############

# SDLS
###############
mkdir Output
for i in `seq 1 $AmountOfIterations`;
do
	python -m pyage.core.bootstrap conf.mememas_sdls $AC $MT $MOI $PRS $MP
	FILE1=$(find . -maxdepth 1 -name "*.txt" -not -name "_stdout.txt")
	mv $FILE1 Output/sim_$i
done

mv *.log sdls/logs
mv Output sdls/
###############

# TS
###############
mkdir Output
for i in `seq 1 $AmountOfIterations`;
do
	python -m pyage.core.bootstrap conf.mememas_ts $AC $MT $MOI $PRS $MP
	FILE1=$(find . -maxdepth 1 -name "*.txt" -not -name "_stdout.txt")
	mv $FILE1 Output/sim_$i
done

mv *.log ts/logs
mv Output ts/
###############

python $BIN_DIR/plot.py

mv *.png plots

python $BIN_DIR/summary.py  > ./summary

mkdir simulation
mv rmhc simulation
mv sdls simulation
mv ts simulation

mv plots simulation
mv parameters simulation
mv summary simulation

rm -f *.pyc
