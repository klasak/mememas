import random
import sys
from pyage.solutions.evolution.mutation import AbstractMutation
from solutions.mem_evolution.genotype import MemListGenotype

class MemListMutation(AbstractMutation):
	def __init__(self, probability, maxIterations):
		super(MemListMutation, self).__init__(MemListGenotype, 1)
		self.mutationProbability = probability
		self.maxIterations = maxIterations
		self.MIN_TABU = self.maxIterations / 10
		self.EXTRA_TABU = self.maxIterations / 50

	def mutate(self, genotype):
		if random.random() < self.mutationProbability:
			index = random.randint(0, len(genotype.genes)-1)
			genotype.genes[index] = -genotype.genes[index]
			genotype.genesChanged()
		self.optimize(genotype)

	def optimize(self, genotype):
		tabu = [0 for x in range(len(genotype.genes))]
		minOfAll = genotype.energy
		minIndex = -1
		for iteration in range(0, self.maxIterations):
			minEnergy = sys.float_info.max
			for i in range(0,len(genotype.genes)-1):
				candidateEnergy = genotype.flip(i)
				if (iteration >= tabu[i] or candidateEnergy < minOfAll):
					if (candidateEnergy < minEnergy):
						minEnergy = candidateEnergy
						minIndex = i

			genotype.genes[minIndex] = -genotype.genes[minIndex]
			genotype.genesChanged()
			tabu[minIndex] = iteration + self.MIN_TABU + random.randint(0, self.EXTRA_TABU)
			if (minEnergy < minOfAll):
				minOfAll = minEnergy

