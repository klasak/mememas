import random
from pyage.solutions.evolution.mutation import AbstractMutation
from solutions.mem_evolution.genotype import MemListGenotype

class MemListMutation(AbstractMutation):
	def __init__(self, probability, maxIterations):
		super(MemListMutation, self).__init__(MemListGenotype, 1)
		self.mutationProbability = probability
		self.maxIterations = maxIterations

	def mutate(self, genotype):
		if random.random() < self.mutationProbability:
			index = random.randint(0, len(genotype.genes)-1)
			genotype.genes[index] = -genotype.genes[index]
			genotype.genesChanged()
		self.optimize(genotype)

	def optimize(self, genotype):
		for iteration in range(0, self.maxIterations):
			index = random.randint(0, len(genotype.genes)-1)
			candidateEnergy = genotype.flip(index)
			if (candidateEnergy < genotype.energy):
				genotype.genes[index] = -genotype.genes[index]
				genotype.genesChanged()

