#!/usr/bin/env python
# encoding: utf-8

from os import listdir
from plot.data import parse_file

out_dir = 'Output/'
rmhc_dir = 'rmhc/' + out_dir
sdls_dir = 'sdls/' + out_dir
ts_dir = 'ts/' + out_dir

f = open('parameters')
parameters = eval(f.read())
f.close()


def format_csv(dict, *keys):
	return ';'.join(str(dict[k]) for k in keys)

def process(**dirs):
	for name, dir in dirs.iteritems():
		for f in listdir(dir):
			path = dir + f
			data = list(parse_file(path))
			vals = [v for (_, v) in data]
			yield dict({
				'type': name,
				'sim': f,
				'best_found': min(vals),
			}, **parameters)

fields = [
	'type', 'agents_count', 'problem_size', 'mutation_probability',
	'max_time', 'max_opt_iterations', 'sim', 'best_found'
]

for line in process(rmhc=rmhc_dir, sdls=sdls_dir, ts=ts_dir):
	print format_csv(line, *fields)
