# coding=utf-8
import logging
import os
import sys

from pyage.core import address

from pyage.core.agent.agent import unnamed_agents
from pyage.core.agent.aggregate import AggregateAgent
from pyage.core.emas import EmasService
from pyage.core.locator import TorusLocator
from pyage.core.migration import ParentMigration
from core.statistics import TimeStatistics
from core.stop_condition import TimeLimitStopCondition
from solutions.mem_evolution.evaluation import MemLABSEvaluation
from solutions.mem_evolution.crossover import SinglePointListCrossover
from solutions.mem_evolution.initializer import mem_list_emas_initializer
from solutions.mem_evolution.mutation_sdls import MemListMutation

if len(sys.argv) == 7:
	agents_count = int(sys.argv[2])
	max_time = int(sys.argv[3])
	max_opt_iterations = int(sys.argv[4])
	problem_size = int(sys.argv[5])
	mutation_probability = float(sys.argv[6])
else:
	agents_count = int(os.environ['AGENTS'])
	max_time = 10
	max_opt_iterations = 10
	problem_size = 100
	mutation_probability = 0.5

minimal_energy = lambda: 0
reproduction_minimum = lambda: 80
migration_minimum = lambda: 120
new_agent_energy = 100
newborn_energy = lambda: new_agent_energy
transferred_energy = lambda: 40
population_size = 100
torus_x = 12


logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

logger.debug("EMAS, %s agents", agents_count)
agents = unnamed_agents(agents_count, AggregateAgent)

stop_condition = lambda: TimeLimitStopCondition(max_time)

aggregated_agents = lambda: mem_list_emas_initializer(dims=problem_size, energy=new_agent_energy, size=population_size)

emas = EmasService

evaluation = MemLABSEvaluation
crossover = lambda: SinglePointListCrossover(population_size)
mutation = lambda: MemListMutation(mutation_probability, max_opt_iterations)

address_provider = address.SequenceAddressProvider
params = lambda: 'AC{0}ME{1}RM{2}MM{3}NAE{4}TE{5}MT{6}MOI{7}PRS{8}POS{9}MP{10}'.format(agents_count(), minimal_energy(), reproduction_minimum(), migration_minimum(), new_agent_energy(), transferred_energy(), max_time(), max_opt_iterations(), problem_size(), population_size(), mutation_probability())

migration = ParentMigration
locator = lambda: TorusLocator(x=torus_x, y=torus_x)

stats = lambda: TimeStatistics()
