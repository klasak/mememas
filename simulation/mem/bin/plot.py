#!/usr/bin/env python
# encoding: utf-8

from plot.data import get_data_from_dir, process_data
from plot.plot import plot_data
from plot.clusterization import partition


out_dir = 'Output/'
rmhc_dir = 'rmhc/' + out_dir
sdls_dir = 'sdls/' + out_dir
ts_dir = 'ts/' + out_dir

f = open('parameters')
parameters = eval(f.read())
f.close()
print parameters

def prepare(data_dir):
	data, k = get_data_from_dir(data_dir)
	# data.sort(key=lambda (t, _): t)
	parts = partition(data, k)
	return list(process_data(parts))


rmhc = prepare(rmhc_dir)
sdls = prepare(sdls_dir)
ts = prepare(ts_dir)

plot_data(parameters, rmhc=rmhc, sdls=sdls, ts=ts)
