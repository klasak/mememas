from os import listdir
from math import sqrt
from sys import maxint

def parse_file(f):
	with open(f, 'r') as src:
		for line in src.readlines():
			time_str, fit_str = line.split(';')
			time = float(time_str)
			fit = float(fit_str)
			yield (time, fit)


def get_data(files):
	data = []
	min_len = maxint
	for f in files:
		vals = list(parse_file(f))
		data.extend(vals)
		min_len = min(min_len, len(vals))
	return (data, min_len)

def get_data_from_dir(data_dir):
	files = listdir(data_dir)
	return get_data(data_dir + f for f in files)


def mean_and_stdev(data):
	mean = sum(data) / float(len(data))
	dev = sqrt(sum((mean - x)**2 for x in data) / float(len(data)))
	return (mean, dev)


def process_data(data):
	for t, vals in data:
		m = min(vals)
		M = max(vals)
		mean, dev = mean_and_stdev(vals)
		yield (t, m, M, mean, dev)
