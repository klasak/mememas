import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt

def plot_data(params, **data):
	fig, ax = plt.subplots(figsize=(12, 7))
	title = 'Rozmiar problemu = {problem_size}'.format(**params)

	ax.set_xlabel('time')
	ax.set_ylabel('fitness')
	ax.set_title(title)
	ax.grid(True)

	colors = ('b', 'r', 'g')

	for col, (label, vals) in zip(colors, data.items()):
		xs, m, M, ys, dev = zip(*vals)
		ax.plot(xs, m, '--', color=col)
		ax.plot(xs, M, '--', color=col)
		ax.plot(xs, ys, '.-', linewidth=2, label=label, color=col)
		ax.errorbar(xs, ys, yerr=dev, fmt='.', color=col)

	ax.legend()
	fig.savefig('full.png', dpi=100)

	#####################################################

	fig, ax = plt.subplots(figsize=(12, 7))

	ax.set_xlabel('time')
	ax.set_ylabel('fitness')
	ax.set_title(title)
	ax.grid(True)

	for label, vals in data.items():
		xs, m, M, ys, dev = zip(*vals)
		ax.plot(xs, ys, '.-', linewidth=2, label=label)

	ax.legend()
	fig.savefig('means.png', dpi=100)

	#####################################################


	for label, vals in data.items():
		fig, ax = plt.subplots(figsize=(12, 7))

		ax.set_xlabel('time')
		ax.set_ylabel('fitness')
		ax.set_title(label + '\n' + title)
		ax.grid(True)

		xs, m, M, ys, dev = zip(*vals)
		ax.plot(xs, ys, '.-', linewidth=2)
		ax.plot(xs, m, '--')
		ax.plot(xs, M, '--')
		ax.errorbar(xs, ys, yerr=dev, fmt='.')

		fig.savefig('{}.png'.format(label), dpi=100)
